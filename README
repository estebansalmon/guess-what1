= Guess What

Prise en main de la POO avec PHP

Niveau : Deuxième année de BTS SIO SLAM 

Prérequis : bases de la programmation, PHP 7 ou supérieur installé

== Thème 

Développer une logique de jeu puis l'adapter progressivement (en _refactoring_) à un contexte d'application web (avec symfony)   
 
Les étapes d'un scénario typique d'usage sont 

1. (optionnel pour le joueur) paramétrage du jeu (par exemple choix du jeu de cartes, aide à la recherche, ...)
2. Lancement d'une partie (le jeu tire une carte "au hasard"), que le joueur doit deviner en un temps "optimal"
3. Le joueur propose une carte  
4. Si c'est la bonne carte alors la partie se termine et le jeu affiche des éléments d'analyse (nombre de fois
 où le joueur a soumis une carte, sa qualité stratégique, ...)
* Si ce n'est pas la bonne carte, alors si l'aide est activée, le joeur est informé si la carte qu'il a soumise est 
plus petite ou plus grande que celle à deviner. Retour en 3.

== Objectif

* Mise au point de la logique applicative avec PHPUnit
* Notion de structure de données, recherche d'un élement dans une liste 
* Analyse des actions du joueur (fonction du nombre de cartes, aides à la décision)  

== Premiers éléments d'analyse

[plantuml]
----
class Guess {
  withHelp : boolean

  start()
  processUserProposal(c : Card)
  getStatistic()
}

class Card {
  name
  color
  {static} cmp(Card c1, Card c2) : int
}

Guess -> "\nselectedCard  1  " Card : "                             "
Guess -> "cards *  " Card : "                                     "

hide circle
----

Cette analyse est une première ébauche, donc incomplète et à ajuster, mais suffisante pour réaliser 
vos premiers pas sur ce projet.

Pour l'essentiel (le cours associé apportera d'autres informations et répondra à vos questions) :

* La classe `Guess` est responsable de la logique du jeu.
* La classe `Card` définit la structure d'une carte à jouer et ses méthodes.

Une instance de `Guess` est reliée, à un instant _t_, à un ensemble de cartes
 (`cards`) et à une instance de `Card` (`selectedCard` est la carte que le joueur doit deviner)

== Première implémentation

Classe `Card` (extrait. Localisation : `src/Core/Card.php`)

[, php]
----
<?php
namespace App\Core;

/**
 * Class Card : Définition d'une carte à jouer
 * @package App\Core
 */
class Card
{
  /**
   * @var $name string nom de la carte, comme par exemples 'As' '2' 'Reine'
   */
  private $name;

  /**
   * @var $color string couleur de la carte, par exemples 'Pique', 'Carreau'
   */
  private $color; 

  /**
   * @return string
   */
  public function getName(): string
  {
    return $this->name;
  }

  // [...]
}
----


Classe `Guess` (incomplète. Localisation : `src/Core/Guess.php`)

[, php]
----
<?php

namespace App\Core;

/**
 * Class Guess : la logique du jeu.
 * @package App\Core
 */
class Guess
{
  /**
   * @var $cards array of Cards
   */
  private $cards;

  /**
   * @var $selectedCard Card This is the card to be guessed by the player
   */
  private $selectedCard;

}
----

== TP1 de prise en main

=== Vérifier les prérequis de votre système

* `php cli`   opérationnel.
* `composer` opérationnel. 

=== Télécharger le projet de démarrage



Une fois cloné, **aller à la racine du projet** puis lancer les commandes suivantes :

* `composer install`  (le téléchargement et installation des composants déclarés dans le fichier `composer.json` peut prendre quelques minutes)

* `./bin/phpunit --version` (le premier lancement de cette commande provoquera l'installation du plugin `phpunit`, puis lancera les tests.
  Le résultat devrait être, à un numéro de version prêt : `PHPUnit 7.5.20 by Sebastian Bergmann and contributors.` )

TIP: Sous windows la commande est `php .\bin\phpunit --version` (remarquez l'usage de `\` au lieu de `/` )



=== Tester le bon fonctionnement de ce petit existant

==== Lancement des tests unitaires
  
À **la racine du projet** du projet, lancer la commande : `./bin/phpunit`

Le résultat attendu est : 

----
kpu@kpu-x1:~/PhpstormProjects/GuessThat$ ./bin/phpunit
PHPUnit 7.5.20 by Sebastian Bergmann and contributors.

Testing Project Test Suite
..RRR                                                               5 / 5 (100%)

Time: 51 ms, Memory: 6.00 MB

There were 3 risky tests: This test did not perform any assertions

1) App\Tests\Core\CardTest::testColor
2) App\Tests\Core\CardTest::testCmp
3) App\Tests\Core\CardTest::testToString

OK, but incomplete, skipped, or risky tests!
Tests: 5, Assertions: 3, Risky: 3.
----


== TP2 implémentation des TODOs 

public function testCmp()
    {
        $card1 = new Card('As', 'Trèfle');
        $card2 = new Card('2', 'Trèfle');
        $this->assertEquals('1', card::cmp($card1, $card2));
    }
        //permet de cree deux cartes et de les comparer ensemble pour voir si l'as est plus fort que le 2

public function testCmpDiffColor()
    {
        $card1 = new Card('As', 'Trèfle');
        $card2 = new Card('As', 'Pique');
        $this->assertEquals('-1', card::cmp($card1, $card2));
    } //cree deux cartes de meme valeur attendu que le pique batte le trèfles

        public function testColor()
    {
        $card1 = new Card('As', 'Trèfle');
        $this->assertEquals('Trèfle', $card1->getColor());
        $card2 = new Card('2', 'Trèfle');
        $this->assertEquals('Trèfle', $card2->getColor());

    }// permet de tester la couleur d'une carte 


public static function cmp(Card $o1, Card $o2) : int

      {
          $o1Name = $o1->name;
          $o2Name = $o2->name;

          $o1Color = $o1->color;
          $o2Color = $o2->color;

          if ($o1Name == $o2Name && $o1Color == $o2Color) {
              return 0;
          }

          $cardNamesNumber = ["As" => 12, "roi" => 11, "dame" => 10, "valet" => 9, "10" => 8, "9" => 7, "8" => 6, "7" => 5, "6" => 4, "5" => 3, "4" => 2, "3" => 1, "2" => 0];
          $cardColorsNumber = ["Pique" => 3, "carreau" => 2, "coeur" => 1, "Trèfle" => 0];

          if ($cardColorsNumber[$o1Color] > $cardColorsNumber[$o2Color]) {
              if ($cardNamesNumber[$o1Name] == $cardNamesNumber[$o2Name]) {
                  return +1;
              }
              return ($cardNamesNumber[$o1Name] > $cardNamesNumber[$o2Name]) ? +1 : -1;
          } else {
              if ($cardNamesNumber[$o1Name] == $cardNamesNumber[$o2Name]) {
                  return -1;
              }
              return ($cardNamesNumber[$o1Name] > $cardNamesNumber[$o2Name]) ? +1 : -1;
          }

      }//tableau 


Voici un extrait de la classe de test :

[, php]
----
<?php

namespace App\Tests\Core; <1>

use PHPUnit\Framework\TestCase;
use App\Core\Card;

class CardTest extends TestCase <2>
{

  public function testName() <3>
  {
    $card = new Card('As', 'Trèfle');  <4>
    $this->assertEquals('As', $card->getName()); <5>
  }



Pour consulter la liste des TODOs, ouvrir la fenêtre TODO tool: `View | Tool Windows | TODO`.
 

== TP3 conception de tests unitaires pour `Guess`

À ce niveau là, vous avez acquis une certaine autonomie sur le projet et intégré les
concepts de base de la notion de tests unitaires. C'est ce que nous allons vérifier.

Votre mission consiste à concevoir une classe de tests qui teste la logique du jeu (représentée par la classe `Guess`).
Ce travail est à réaliser en binôme. Il y aura des décisions à prendre, qui pourront être discuter collectivement, 
entre différents binômes.

Voici quelques éléments à prendre en compte dans votre analyse.

* Recherche linéaire (dite aussi séquentielle) : L'utilisateur explore une à une les cartes afin de trouver la bonne. 
Dans le pire cas il soumettra autant de cartes que le jeu en contient (l'ordre de grandeur est O(n), _n_ étant 
le nombre de cartes), dans le meilleur cas O(1) (coup de chance il tombe dessus du premier coup).  
* Recherche dichotomique (nécessite une relation d'ordre total) : Si l'utilisateur est informé de la position de 
la carte qu'il soumet par rapport à la carte à trouver (inférieur ou supérieur) alors il peut appliquer une 
stratégie qui réduit le nombre de cas à soumettre dans le pire cas, de l'ordre de O(log2 n). Wikipédia vous fournira
des informations utiles sur ces notions.

L'analyse de la stratégie du joueur, lorsqu'il termine une partie, devra prendre en compte les paramètres du jeu, 
à savoir le nombre de cartes et l'aide à la décision si elle a été activée pour la partie en question.

L'analyse de la stratégie du joueur peut être représentée sous la forme d'un texte (une chaine de caractères). C'est à
vous de décider de son contenu (sa valeur).



== Livraison

Modalité de livraison (mode « trio ») :Salmon esteban,Bennoua brice,Sayah omar

lien du Project : https://gitlab.com/estebansalmon/guess-what1

