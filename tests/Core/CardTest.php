<?php

namespace App\Tests\Core;

use PHPUnit\Framework\TestCase;
use App\Core\Card;

class CardTest extends TestCase
{

    public function testName()
    {
        $card1 = new Card('As', 'Trèfle');
        $this->assertEquals('As', $card1->getName());
        $card2 = new Card('2', 'Trèfle');
        $this->assertEquals('2', $card2->getName());

    }

    public function testSort()
    {
        $cards = [];

        $card1 = new Card('As', 'Trèfle');
        $cards[] = $card1;
        $card2 = new Card('2', 'Pique');
        $cards[] = $card2;

        // vérifie que la première carte est bien un As
        $this->assertEquals('As', $cards[0]->getName());

        // trie le tableau $cards, en utilisant la fonction de comparaison Card::cmp
        // rem : la syntaxe n'est pas intuitive, on doit passer
        // le nom complet de la classe et le nom d'une méthode de comparaison.
        // (voir https://www.php.net/manual/fr/function.usort.php)
        usort($cards, array("App\Core\Card", "cmp"));

        // vérifie que le tableau $cards a bien été modifié par usort
        // dans la table ASCII, les chiffres sont placés avant les lettres de l'alphabet
        $this->assertEquals('2', $cards[0]->getName());
    }

    public function testColor()
    {
        $card1 = new Card('As', 'Trèfle');
        $this->assertEquals('Trèfle', $card1->getColor());
        $card2 = new Card('2', 'Trèfle');
        $this->assertEquals('Trèfle', $card2->getColor());

    }

    public function testCmp()
    {
        $card1 = new Card('As', 'Trèfle');
        $card2 = new Card('2', 'Trèfle');
        $this->assertEquals('1', card::cmp($card1, $card2));
        //coucou

    }

    public function testCmpDiffColor()
    {
        $card1 = new Card('As', 'Trèfle');
        $card2 = new Card('As', 'Pique');
        $this->assertEquals('-1', card::cmp($card1, $card2));
    }

    public function testToString()
    {
        $card1 = new Card('As', 'Trèfle');
        $this->assertEquals("As Trèfle", $card1->toString());


    }
}